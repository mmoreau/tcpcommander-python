#!/usr/bin/python3

import socket
import datetime
import platform as plf

def recvall(sock, buffer=4096):

    """Recover all data from the buffer."""

    if isinstance(buffer, int):
        if buffer > 0:

            buf = sock.recv(buffer)

            while buf:
                yield buf

                if len(buf) < buffer:
                    break

                buf = sock.recv(buffer)


os = plf.system()
print("Started client :", datetime.datetime.now().strftime("%d/%m/%Y - %H:%M:%S"))

# Creating an IPv4, TCP socket.
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    # Try to connect to the remote server.
    try:
        # Connection to the server with Host Name & Port.
        s.connect(("127.0.0.1", 1337))

        # Sends orders in real time
        while 1:
            try:
                # Request to the user for data entry.
                dataIn = input("\n>>> ")

                if isinstance(dataIn, str):
                    if dataIn: 
                        
                        # Encodes the user's data.
                        s.send(dataIn.encode())

                        # Close the client or recover the data.
                        if dataIn in ("exit", "quit", "bye"):
                            print("\nFinished client :", datetime.datetime.now().strftime("%d/%m/%Y - %H:%M:%S"), "\n")
                            break
                        else:
                            # Recover data from the server.
                            dataOut = b"\n".join(recvall(s))

                            # Retrieves only order data.
                            if dataOut != b" ":

                                if os == "Windows":
                                    print(dataOut.decode("utf-8", "ignore"))
                                elif os in ("Linux", "Darwin"):
                                    [print(result) for result in dataOut.decode().split("\n") if result]
            except:
                pass
    except ConnectionRefusedError:
        print("\nConnection refused")