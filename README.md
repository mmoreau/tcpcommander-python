# TCPCommander
**Sends** and **retrieves** commands from the **remote server**.

You can rename the files "**py**" to "**pyw**" to avoid displaying the console, it is ideal to launch as a service when starting the machine.

## Dependencies

* Python 3 (https://www.python.org/downloads/)

## Terminal

### **Windows**
<pre>
    # Terminal n°1
    <b>python server.py</b> or <b>server.py</b>
    
    # Terminal n°2
    <b>python client.py</b> or <b>client.py</b>
</pre>

### **Linux & Mac**
<pre>
    # Terminal n°1
    <b>python3 server.py</b>
    
    # Terminal n°2
    <b>python3 client.py<b/>
</pre>

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/